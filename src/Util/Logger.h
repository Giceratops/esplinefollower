#ifndef LOGGER_H
#define LOGGER_H

// supports 16 bit (0 is always ignored)
//                 (1 is always sent)
#define LOG_FLAG_EVENT    0b00000001 // 1
#define LOG_FLAG_MAIN     0b00000010 // 2
#define LOG_FLAG_SENSOR   0b00000100 // 4
#define LOG_FLAG_PID      0b00001000 // 8
#define LOG_FLAG_ENGINE   0b00010000 // 16

// supports [0, 9] (0 is always ignored)
#define LOG_LEVEL_SILENT  0
#define LOG_LEVEL_ERROR   1
#define LOG_LEVEL_WARNING 2
#define LOG_LEVEL_INFO    3
#define LOG_LEVEL_DEBUG   4
#define LOG_LEVEL_VERBOSE 5

#include "Arduino.h"
#include "AsyncWebSocket.h"
#include "ArduinoJson.h"

#include "util/Settings.h"

#include "parts/Sensor.h"
#include "parts/PID.h"
#include "parts/Engine.h"

class Logger {
private:
    Settings * settings;
    AsyncWebSocket * ws;

    int maskLevel(int mask);
    bool accept(int mask, int level);

    void appendLog(JsonObject & obj, int mask, int level);

public:
    Logger(Settings * settings, AsyncWebSocket * ws);
    static Logger& get();

    void handshake(AsyncWebSocketClient * client = NULL);

    void log(JsonObject & obj, AsyncWebSocketClient * client = NULL, bool broadcast = false);
    void log(int flag, int level, const char * msg, AsyncWebSocketClient * client = NULL, bool broadcast = false);

    void info(const Sensor::Output & sens, int massMax);
    void info(const Engine::Output & eng);
    void info(const PID::Output & pid);
};

#endif // ifndef LOGGER_H
