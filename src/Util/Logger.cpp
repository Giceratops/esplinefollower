#include "Logger.h"

Logger::Logger(Settings * settings, AsyncWebSocket * ws) {
    this->settings = settings;
    this->ws = ws;
}

int Logger::maskLevel(int mask) {
    long tmp = this->settings->log();

    for (byte b = 0; b < 16; b++) {
        if (bitRead(mask, b) == 1) {
            return tmp % 10;
        }
        tmp /= 10;
    }
    return 0;
}

bool Logger::accept(int mask, int level) {
    if (level <= 0) {
        return false;
    } else {
        int msk = this->maskLevel(mask);
        return msk == 0 ? false : msk >= level;
    }
}

void Logger::appendLog(JsonObject& obj, int mask, int level) {
    obj["mask"] = mask;
    obj["level"] = level;
}

void Logger::log(JsonObject & obj, AsyncWebSocketClient * client, bool broadcast) {
    if (client) {
        obj["con"] = client->id();
    }

    size_t len = obj.measureLength();
    AsyncWebSocketMessageBuffer * buffer = this->ws->makeBuffer(len);

    if (buffer) {
        obj.printTo((char *) buffer->get(), len + 1);
        if (broadcast || !client) {
            this->ws->textAll(buffer);
        } else if (client) {
            client->text(buffer);
        }
    }
}

void Logger::handshake(AsyncWebSocketClient * client) {
    DynamicJsonBuffer jsonBuffer;
    JsonObject& obj = jsonBuffer.createObject();

    this->appendLog(obj, LOG_FLAG_EVENT, LOG_LEVEL_INFO);

    if (client) {
        obj["con"] = client->id();
        obj["handshake"] = true;
    }

    JsonObject & events = obj.createNestedObject("events");
    JsonObject & ip = events.createNestedObject("ip");
    this->settings->appendToJSON(&events);
    this->settings->appendHTTPToJSON(&ip);

    size_t len = obj.measureLength();
    AsyncWebSocketMessageBuffer * buffer = this->ws->makeBuffer(len);

    if (buffer) {
        obj.printTo((char *) buffer->get(), len + 1);

        if (client) {
            client->text(buffer);
        } else {
            this->ws->textAll(buffer);
        }
    }
} // Logger::handshake

void Logger::log(int mask, int level, const char * msg, AsyncWebSocketClient * client, bool broadcast) {
    if (this->accept(mask, level)) {
        DynamicJsonBuffer jsonBuffer;
        JsonObject& obj = jsonBuffer.createObject();
        this->appendLog(obj, mask, level);
        obj["msg"] = msg;
        this->log(obj, client, broadcast);
    }
}

void Logger::info(const Sensor::Output & sens, int massMax) {
    if (this->accept(LOG_FLAG_SENSOR, LOG_LEVEL_INFO)) {
        DynamicJsonBuffer jsonBuffer;
        JsonObject& obj = jsonBuffer.createObject();
        this->appendLog(obj, LOG_FLAG_SENSOR, LOG_LEVEL_INFO);
        JsonArray& data = obj.createNestedArray("data");
        for (byte b = 0; b < SENSOR_COUNT; b++) {
            data.add(sens.values[b]);
        }
        obj["centroid"] = sens.centroid;
        obj["mass"] = sens.mass;
        obj["massMax"] = massMax;
        this->log(obj);
    }
}

void Logger::info(const Engine::Output & eng) {
    if (this->accept(LOG_FLAG_ENGINE, LOG_LEVEL_INFO)) {
        DynamicJsonBuffer jsonBuffer;
        JsonObject& obj = jsonBuffer.createObject();
        this->appendLog(obj, LOG_FLAG_ENGINE, LOG_LEVEL_INFO);
        obj["left"] = eng.left;
        obj["right"] = eng.right;
        this->log(obj);
    }
}

void Logger::info(const PID::Output & pid) {
    if (this->accept(LOG_FLAG_PID, LOG_LEVEL_INFO)) {
        DynamicJsonBuffer jsonBuffer;
        JsonObject& obj = jsonBuffer.createObject();
        this->appendLog(obj, LOG_FLAG_PID, LOG_LEVEL_INFO);
        obj["in"] = pid.in;
        obj["out"] = pid.out;
        this->log(obj);
    }
}
