#include "CommandHandler.h"

CommandHandler::CommandHandler(Settings * settings, Logger * logger) {
    this->settings = settings;
    this->logger = logger;
}

CommandHandler::WebControl CommandHandler::webRC() {
    return this->webControl;
}

char * CommandHandler::next(){
    return strtok_r(NULL, " ", &last);
}

void CommandHandler::handleCommand(AsyncWebSocketClient * client, uint8_t * data) {
    DynamicJsonBuffer jsonBuffer;
    JsonObject & obj = jsonBuffer.createObject();

    char * cmd = strtok_r((char *) data, " ", &last);
    bool ret = false;

    if (cmd == NULL) {
    } else if (strcmp(cmd, "SET") == 0) {
        ret = this->onSet(obj);
    } else if (strcmp(cmd, "MOD") == 0) {
        char * value = strtok_r(NULL, " ", &last);
        if (value != NULL) {
            int id = -1;
            if (client) {
                id = client->id();
            }
            JsonObject & events = obj.createNestedObject("events");
            events["mod"] =  this->settings->mode(atoi(value), id);
            ret = true;
        }
    } else if (strcmp(cmd, "CAL") == 0) {
        this->settings->calibrate(true);
    } else if (strcmp(cmd, "MAN") == 0) {
        this->webControl.left = atoi(this->next());
        this->webControl.right = atoi(this->next());
    } else if (strcmp(cmd, "STR") == 0) {
        ret = this->onStore(obj);
    } else {
        this->logger->log(LOG_FLAG_MAIN, LOG_LEVEL_WARNING, "Invalid command.", client);
    }

    if (ret) {
        this->logger->log(obj, client, true);
    }
} // CommandHandler::handleCommand

bool CommandHandler::onSet(JsonObject& obj) {
    JsonObject & events = obj.createNestedObject("events");
    char * what = this->next();

    if (what == NULL) {
    } else if (strcmp(what, "pf") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->powerFactor(atoi(value));
            return true;
        }
    } else if (strcmp(what, "kp") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->kp(atof(value));
            return true;
        }
    } else if (strcmp(what, "ki") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->ki(atof(value));
            return true;
        }
    } else if (strcmp(what, "kd") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->kd(atof(value));
            return true;
        }
    } else if (strcmp(what, "log") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->log(atol(value));
            return true;
        }
    } else if (strcmp(what, "pullup") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->pullUp(atoi(value) == 1);
            return true;
        }
    } else if (strcmp(what, "smoothoffset") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->smoothOffset(atoi(value));
            return true;
        }
    } else if (strcmp(what, "smoothmin") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->smoothMin(atoi(value));
            return true;
        }
    } else if (strcmp(what, "usepid") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->pid(atoi(value) == 1);
            return true;
        }
    } else if (strcmp(what, "reverse") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->reverse(atoi(value) == 1);
            return true;
        }
    } else if (strcmp(what, "ip") == 0) {
        char * json = this->next();
        DynamicJsonBuffer jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(json);

        root.set("ftpUser", settings->http.ftpUser);
        root.set("ftpPass", settings->http.ftpPass);

        SPIFFS.remove(FILE_HTTP);
        File f = SPIFFS.open(FILE_HTTP, "w+");
        root.printTo(f);
        f.close();

        JsonObject & ip = events.createNestedObject("ip");
        settings->loadHTTP(&root);
        settings->appendHTTPToJSON(&ip);

        return true;
    } else if (strcmp(what, "highthreshold") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->highThreshold(atoi(value));
            return true;
        }
    } else if (strcmp(what, "highdetections") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->highDetections(atoi(value));
            return true;
        }
    } else if (strcmp(what, "lowthreshold") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->lowThreshold(atoi(value));
            return true;
        }
    } else if (strcmp(what, "lowdetections") == 0) {
        char * value = this->next();
        if (value != NULL) {
            events[what] = this->settings->lowDetections(atoi(value));
            return true;
        }
    }
    return false;
} // CommandHandler::onSet

bool CommandHandler::onStore(JsonObject& obj) {
    char * what = this->next();

    JsonObject & str = obj.createNestedObject("str");

    if (what == NULL) {
        JsonArray & arr = str.createNestedArray("data");

        Dir dir = SPIFFS.openDir("/str/");
        while (dir.next()) {
            File file = dir.openFile("r");
            DynamicJsonBuffer buf;

            JsonObject& obj = arr.createNestedObject();
            JsonObject& json = buf.parse(file);

            obj["fileName"] = dir.fileName();
            obj["pf"] = json["pf"];
            obj["kp"] = json["kp"];
            obj["ki"] = json["ki"];
            obj["kd"] = json["kd"];
            obj["speed"] = json["speed"];
            obj["desc"] = json["desc"];

            file.close();
        }
    } else if (strcmp(what, "save") == 0) {
        char * fileName = this->next();

        if (strcmp(fileName, "/str/") == 1) {
            str["msg"] = "Filename must start with /str/";
            str["msgType"] = LOG_LEVEL_ERROR;
        } else if (strlen(fileName) > 31) {
            str["msg"] = "Filename is too long.";
            str["msgType"] = LOG_LEVEL_ERROR;
        } else {

            SPIFFS.remove(fileName);
            File file = SPIFFS.open(fileName, "w+");

            if (!file) {
                JsonObject & str = obj.createNestedObject("str");
                str["msg"] = "Unable to open file";
                str["msgType"] = LOG_LEVEL_ERROR;
            } else {

                DynamicJsonBuffer jsonBuffer;
                JsonObject& json = jsonBuffer.createObject();

                json["pf"] = settings->powerFactor();
                json["kp"] = settings->kp();
                json["ki"] = settings->ki();
                json["kd"] = settings->kd();
                json["smoothmin"] = settings->smoothMin();
                json["smoothoffset"] = settings->smoothOffset();
                json["speed"] = atof(this->next());
                json["desc"] = this->next();

                json.printTo(file);
                file.close();

                if (json.success()) {
                    str["msg"] = "Successfully saved";
                    str["save"] = fileName;
                    str["msgType"] = LOG_LEVEL_INFO;
                } else {
                    str["msg"] = "Error saving, try again";
                    str["msgType"] = LOG_LEVEL_ERROR;
                }
            }
        }
    } else if (strcmp(what, "load") == 0) {

        char * fileName = this->next();

        if (SPIFFS.exists(fileName)) {
            File file = SPIFFS.open(fileName, "r");
            if (!file) {
                str["msg"] = "Unable to read file";
                str["msgType"] = LOG_LEVEL_ERROR;
            } else {
                DynamicJsonBuffer jsonBuff;
                JsonObject& json = jsonBuff.parse(file);

                if (!json.success()) {
                    str["msg"] = "Unable to parse json";
                    str["msgType"] = LOG_LEVEL_ERROR;
                } else {
                    settings->set(json["pf"], json["kp"], json["ki"], json["kd"], json["smoothoffset"], json["smoothmin"]);
                    str["msg"] = "Successfully loaded settings";
                    str["msgType"] = LOG_LEVEL_INFO;

                    JsonObject & events = obj.createNestedObject("events");
                    events["pf"] = json["pf"];
                    events["kp"] = json["kp"];
                    events["ki"] = json["ki"];
                    events["kd"] = json["kd"];
                    events["smoothmin"] = json["smoothmin"];
                    events["smoothoffset"] = json["smoothoffset"];
                }
            }
        } else {
            str["msg"] = "File no longer found";
            str["msgType"] = LOG_LEVEL_ERROR;
        }
    } else if (strcmp(what, "rm") == 0) {
        char * fileName = this->next();

        if (fileName == NULL) {
            str["msg"] = "Use STR rm <filename>";
            str["msgType"] = LOG_LEVEL_ERROR;
        } else if (strcmp(fileName, "/str/") == 1) {
            str["msg"] = "Filename must start with /str/";
            str["msgType"] = LOG_LEVEL_ERROR;
        } else {
            SPIFFS.remove(fileName);
            str["msg"] = "Removed file";
            str["rm"] = fileName;
            str["msgType"] = LOG_LEVEL_INFO;
        }
    }

    return true;
} // CommandHandler::onStore
