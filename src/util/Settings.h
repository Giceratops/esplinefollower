#ifndef SETTINGS
#define SETTINGS

#define SENSOR_COUNT  7

#define SerialPort    Serial

#define RANGE_PWM     1023
#define RANGE_LINE    1000
#define PID_SETPOINT  RANGE_LINE / 2

// WARNING max length of path name is 31 characters
//                    "_______________________________"
#define FILE_SETTINGS "/props/settings.json"
#define FILE_STORE    "/str/default.json"
#define FILE_HTTP     "/props/http.json"

#include "Arduino.h"
#include "ArduinoJson.h"
#include "FS.h"

class Settings {
private:
    const char * _fileName;

    // stored values
    byte _pf;
    float _kp, _ki, _kd;
    long _log;
    bool _pullUp, _usePID, _reverse;
    int _smoothOffset, _smoothMin;
    int _highDetections, _highThreshold;
    int _lowDetections, _lowThreshold;
    int _cal[SENSOR_COUNT];

    // internal use for program
    bool _calibrate;
    int _controller, _mode;
public:
    struct HTTP {
        String hostName;
        String ssid, pass;
        String ftpUser, ftpPass;
        int ip, gateway, mask;
        byte channel;
    } http;

    Settings(const char * fileName);

    bool load();
    bool loadHTTP(JsonObject * json);
    bool save();

    void appendHTTPToJSON(JsonObject * json, bool pass = false);
    void appendToJSON(JsonObject * json);

    byte powerFactor();
    byte powerFactor(byte pf);

    float kp();
    float kp(float kp);

    float ki();
    float ki(float ki);

    float kd();
    float kd(float kd);

    bool set(byte pf, float kp, float ki, float kd, int smoothOffset, int smoothMin);

    long log();
    long log(long log);

    bool calibrate();
    void calibrate(bool trigger);
    int * calibration();
    int * calibration(int * cal);

    int smoothOffset();
    int smoothOffset(int value);

    int smoothMin();
    int smoothMin(int value);

    bool pullUp();
    bool pullUp(bool enabled);

    int mode(int mode, int controller = -1);
    bool resetModeOnDisconnect(int controller);
    int mode();

    bool pid();
    bool pid(bool enabled);

    bool reverse();
    bool reverse(bool reverse);

    int highDetections();
    int highDetections(int value);

    int highThreshold();
    int highThreshold(int value);

    int lowDetections();
    int lowDetections(int value);

    int lowThreshold();
    int lowThreshold(int value);
};

#endif // ifndef PERSISTANT_SETTINGS
