#ifndef COMMAND_HANDLER_H
#define COMMAND_HANDLER_H

#include "Arduino.h"
#include "ESPAsyncWebServer.h"
#include "ArduinoJson.h"

#include "util/Settings.h"
#include "util/Logger.h"

class CommandHandler {
public:
    struct WebControl {
        int left, right;
    };
    CommandHandler(Settings * settings, Logger * logger);

    void handleCommand(AsyncWebSocketClient * client, uint8_t * data);
    WebControl webRC();

private:
    Settings * settings;
    Logger * logger;

    char * last;
    WebControl webControl;

    char * next();

    bool onSet(JsonObject& obj);
    bool onStore(JsonObject& obj);
};
#endif // ifndef COMMAND_HANDLER_H
