#include "Settings.h"

Settings::Settings(const char * fileName) {
    this->_fileName = fileName;
}

bool Settings::load() {
    File file = SPIFFS.open(FILE_HTTP, "r");

    if (!file) {
        return false;
    }

    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.parse(file);

    if (!loadHTTP(&json)) {
        return false;
    }

    file.close();

    file = SPIFFS.open(this->_fileName, "r");

    if (!file) {
        return false;
    }

    JsonObject& json2 = jsonBuffer.parse(file);
    file.close();

    if (!json2.success()) {
        return false;
    }

    this->_pf = json2["pf"];
    this->_kp = json2["kp"];
    this->_ki = json2["ki"];
    this->_kd = json2["kd"];
    this->_log = json2["log"];
    this->_pullUp = json2.get<bool>("pullup");
    this->_smoothOffset = json2.get<int>("smoothoffset");
    this->_smoothMin = json2.get<int>("smoothmin");
    this->_usePID = json2.get<bool>("usepid");
    this->_reverse = json2.get<bool>("reverse");
    this->_highDetections = json2.get<int>("highdetections");
    this->_lowDetections = json2.get<int>("lowdetections");
    this->_highThreshold = json2.get<int>("highthreshold");
    this->_lowThreshold = json2.get<int>("lowthreshold");
    for (byte b = 0; b < SENSOR_COUNT; b++) {
        this->_cal[b] = json2["cal"][b];
    }
} // Settings::load

bool Settings::loadHTTP(JsonObject * json) {
    this->http.hostName = json->get<String>("hostName");
    this->http.ssid = json->get<String>("ssid");
    this->http.pass = json->get<String>("pass");
    this->http.ftpUser = json->get<String>("ftpUser");
    this->http.ftpPass = json->get<String>("ftpPass");
    this->http.channel = json->get<int>("channel");
    this->http.ip = json->get<int>("ip");
    this->http.gateway = json->get<int>("gateway");
    this->http.mask = json->get<int>("mask");

    return json->success();
}

void Settings::appendToJSON(JsonObject * json) {
    JsonArray& cal = json->createNestedArray("cal");

    json->set("pf", this->_pf);
    json->set("kp", this->_kp);
    json->set("ki", this->_ki);
    json->set("kd", this->_kd);
    json->set("log", this->_log);
    json->set("pullup", this->_pullUp);
    json->set("smoothmin", this->_smoothMin);
    json->set("smoothoffset", this->_smoothOffset);
    json->set("usepid", this->_usePID);
    json->set("reverse", this->_reverse);
    json->set("mod", this->_mode);
    json->set("highdetections", this->_highDetections);
    json->set("lowdetections", this->_lowDetections);
    json->set("highthreshold", this->_highThreshold);
    json->set("lowthreshold", this->_lowThreshold);
    for (byte b = 0; b < SENSOR_COUNT; b++) {
        cal.add(this->_cal[b]);
    }
} // Settings::appendToJSON

void Settings::appendHTTPToJSON(JsonObject * json, bool pass) {
    json->set("hostName", this->http.hostName);
    json->set("ssid", this->http.ssid);
    if (pass) {
        json->set("pass", this->http.pass);
    }
    json->set("ftpUser", this->http.ftpUser);
    json->set("ftpPass", this->http.ftpPass);
    json->set("channel", this->http.channel);
    json->set("ip", this->http.ip);
    json->set("gateway", this->http.gateway);
    json->set("mask", this->http.mask);
}

bool Settings::save() {
    if (SPIFFS.exists(this->_fileName)) {
        SPIFFS.remove(this->_fileName);
    }

    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    this->appendToJSON(&json);

    File file = SPIFFS.open(this->_fileName, "w+");
    if (!file) {
        return false;
    }
    json.printTo(file);
    file.close();
    return json.success();
} // Settings::save

byte Settings::powerFactor() {
    return this->_pf;
}

byte Settings::powerFactor(byte pf) {
    this->_pf = constrain(pf, 0, 100);
    this->save();
    return this->_pf;
}

float Settings::kp() {
    return this->_kp;
}

float Settings::kp(float kp) {
    this->_kp = _max(0, kp);
    this->save();
    return this->_kp;
}

float Settings::ki() {
    return this->_ki;
}

float Settings::ki(float ki) {
    this->_ki = _max(0, ki);
    this->save();
    return this->_ki;
}

float Settings::kd() {
    return this->_kd;
}

float Settings::kd(float kd) {
    this->_kd = _max(0, kd);
    this->save();
    return this->_kd;
}

bool Settings::set(byte pf, float kp, float ki, float kd, int smoothOffset, int smoothMin) {
    this->_pf = pf;
    this->_kp = kp;
    this->_ki = ki;
    this->_kd = kd;
    this->_smoothMin = smoothMin;
    this->_smoothOffset = smoothOffset;
    this->save();
}

long Settings::log() {
    return this->_log;
}

long Settings::log(long log) {
    this->_log = _max(0, log);
    this->save();
    return this->_log;
}

int * Settings::calibration() {
    return this->_cal;
}

int * Settings::calibration(int * cal) {
    this->calibrate(false);

    for (byte b = 0; b < SENSOR_COUNT; b++) {
        this->_cal[b] = cal[b];
    }
    this->save();
    return this->_cal;
}

void Settings::calibrate(bool b) {
    this->_calibrate = b;
}

bool Settings::calibrate(){
    return this->_calibrate;
}

int Settings::mode(int mode, int controller) {
    this->_mode = mode;
    this->_controller = controller;
    return this->_mode;
}

int Settings::mode() {
    return this->_mode;
}

bool Settings::resetModeOnDisconnect(int controller) {
    if (this->_mode == 2 && this->_controller == controller) {
        this->_mode = 0;
        return true;
    }
    return false;
}

bool Settings::pid() {
    return this->_usePID;
}

bool Settings::pid(bool enabled) {
    this->_usePID = enabled;
    this->save();
    return this->pid();
}

bool Settings::pullUp() {
    return this->_pullUp;
}

bool Settings::pullUp(bool enabled) {
    this->_pullUp = enabled;
    this->save();
    return this->pullUp();
}

int Settings::smoothMin() {
    return this->_smoothMin;
}

int Settings::smoothMin(int value){
    this->_smoothMin = constrain(value, -1000, 1000);
    this->save();
    return this->smoothMin();
}

int Settings::smoothOffset(){
    return this->_smoothOffset;
}

int Settings::smoothOffset(int value){
    this->_smoothOffset = constrain(value, -1000, 1000);
    this->save();
    return this->smoothOffset();
}

bool Settings::reverse() {
    return this->_reverse;
}

bool Settings::reverse(bool reverse) {
    this->_reverse = reverse;
    this->save();
    return this->reverse();
}

int Settings::highDetections() {
    return this->_highDetections;
}

int Settings::highDetections(int value) {
    this->_highDetections = _max(0, value);
    this->save();
    return this->highDetections();
}

int Settings::highThreshold(){
    return this->_highThreshold;
}

int Settings::highThreshold(int value) {
    this->_highThreshold = constrain(value, 0, SENSOR_COUNT * 1000);
    this->save();
    return this->highThreshold();
}

int Settings::lowDetections(){
    return this->_lowDetections;
}
int Settings::lowDetections(int value) {
    this->_lowDetections = _max(0, value);
    this->save();
    return this->lowDetections();
}

int Settings::lowThreshold(){
    return this->_lowThreshold;
}
int Settings::lowThreshold(int value) {
    this->_lowThreshold = constrain(value, 0, SENSOR_COUNT * 1000);
    this->save();
    return this->lowThreshold();
}
