#include "Arduino.h"
#include "Hash.h"
#include "FS.h"

#include "ESP8266WiFi.h"
#include "ESPAsyncWebServer.h"
#include "SPIFFSEditor.h"

#include "util/Settings.h"
#include "util/Logger.h"
#include "util/CommandHandler.h"

#include "parts/PID.h"
#include "parts/Sensor.h"
#include "parts/Engine.h"

AsyncWebServer server = AsyncWebServer(80);
AsyncWebSocket ws = AsyncWebSocket("/ws");

Settings settings = Settings(FILE_SETTINGS);

Logger logger = Logger(&settings, &ws);

Sensor sensor = Sensor(&settings, D0, D5, D6, A0);
PID pid = PID(&settings);
Engine engine = Engine(&settings, new DCMotor(D4, D3), new DCMotor(D2, D1));

CommandHandler handler = CommandHandler(&settings, &logger);

void handleWebEvent(AsyncWebSocket * server, AsyncWebSocketClient * client,
                    AwsEventType type, void * arg, uint8_t * data, size_t len);
void initButtons();
void onStart();
void onCalibrate();

bool setupComplete = false;

void setup() {
    SerialPort.begin(115200);
    SerialPort.setDebugOutput(true);

    SerialPort.println();
    SerialPort.println();

    if (!SPIFFS.begin()) {
        SerialPort.println("[Fatal] SPIFFS.begin failed");
    } else if (!settings.load()) {
        SerialPort.println("[Fatal] Deserializing settings failed");
    } else if (!WiFi.mode(WIFI_AP)) {
        SerialPort.println("[Fatal] WiFi.mode failed");
    } else if (!WiFi.softAPConfig(settings.http.ip, settings.http.gateway, settings.http.mask)) {
        SerialPort.println("[Fatal] WiFi.softAPConfig failed");
    } else if (!WiFi.softAP(settings.http.ssid.c_str(), settings.http.pass.c_str(), settings.http.channel)) {
        SerialPort.println("[Fatal] WiFi.softAP failed");
    } else {
        initButtons();

        ws.onEvent(handleWebEvent);

        server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");
        server.addHandler(&ws);
        server.addHandler(new SPIFFSEditor());
        server.begin();

        setupComplete = true;
    }
}  // setup

Sensor::Output sensorOut;
PID::Output pidOut;
Engine::Output engineOut;

int maxMass;
long lastUpdate = 0;
int lowDetects, highDetects;

void loop() {
    if (!setupComplete) {
        SerialPort.println("[Fatal] Setup failure... ");
        delay(5000);
        return;
    }

    sensorOut = sensor.read();
    maxMass = _max(maxMass, sensorOut.mass);

    if (settings.mode() == 1) {
        bool stop = false;

        if (sensorOut.mass < settings.lowThreshold()) {
            if (++lowDetects >= settings.lowDetections()) {
                onStart();
            }
        } else {
            lowDetects = 0;
        }

        if (sensorOut.mass > settings.highThreshold()) {
            if (++highDetects >= settings.highDetections()) {
                onStart();
            }
        } else {
            highDetects = 0;
        }
    } else {
        lowDetects = highDetects = 0;
    }


    if (settings.calibrate()) {
        onCalibrate();
    }

    CommandHandler::WebControl ctrl;
    switch (settings.mode()) {
        case 0:
            engineOut = engine.off();
            break;
        case 1:
            pidOut = pid.compute(sensorOut.centroid);
            engineOut = engine.power(pidOut.out);
            break;
        default:
            ctrl = handler.webRC();
            engineOut = engine.power(ctrl.left, ctrl.right);
            break;
    }

    long now = millis();
    if (now - lastUpdate > 200) {
        logger.info(sensorOut, maxMass);
        logger.info(pidOut);
        logger.info(engineOut);
        maxMass = 0;
        lastUpdate = now;
    }
} // loop

void handleWebEvent(AsyncWebSocket * server, AsyncWebSocketClient * client,
                    AwsEventType type, void * arg, uint8_t * data, size_t len) {
    if (type == WS_EVT_CONNECT) {
        logger.handshake(client);
    } else if (type == WS_EVT_DISCONNECT) {
        bool rst = settings.resetModeOnDisconnect(client->id());
        if (rst) {
            handler.handleCommand(client, (uint8_t *) "MOD 0");
        }
    } else if (type == WS_EVT_DATA) {
        AwsFrameInfo * info = (AwsFrameInfo *) arg;
        if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
            handler.handleCommand(client, data);
            for (int i = 0; i < len; i++) {
                data[i] = '\0'; // clear the buffer
            }
        } else {
            logger.log(LOG_FLAG_MAIN, LOG_LEVEL_WARNING, "Ignoring binary/packetized message.");
        }
    }
}

void initButtons() {
    if (setupComplete) {
        detachInterrupt(D7);
        detachInterrupt(D8);
    }

    if (settings.pullUp()) {
        pinMode(D7, INPUT_PULLUP);
        attachInterrupt(digitalPinToInterrupt(D7), onStart, RISING);

        pinMode(D8, INPUT_PULLUP);
        attachInterrupt(digitalPinToInterrupt(D8), onCalibrate, RISING);
    } else {
        pinMode(D7, INPUT);
        attachInterrupt(digitalPinToInterrupt(D7), onStart, FALLING);

        pinMode(D8, INPUT);
        attachInterrupt(digitalPinToInterrupt(D8), onCalibrate, FALLING);
    }
} // initButtons

void onStart() {
    int mod = settings.mode(settings.mode() == 0 ? 1 : 0);

    DynamicJsonBuffer jsonBuffer;
    JsonObject& obj = jsonBuffer.createObject();
    JsonObject& events = obj.createNestedObject("events");

    events.set("mod", mod);
    logger.log(obj, NULL, true);
}

void onCalibrate() {
    settings.calibration(sensorOut.values);

    DynamicJsonBuffer jsonBuffer;
    JsonObject& obj = jsonBuffer.createObject();
    JsonObject& events = obj.createNestedObject("events");
    JsonArray& data = events.createNestedArray("cal");
    for (byte b = 0; b < SENSOR_COUNT; b++) {
        data.add(sensorOut.values[b]);
    }
    logger.log(obj, NULL, true);
}
