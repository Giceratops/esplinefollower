#ifndef SENSOR_H
#define SENSOR_H

#include "Arduino.h"

#include "util/Settings.h"

class Sensor {
private:
    Settings * settings;
    byte pins[3];
    byte analog;

public:
    struct Output {
        int centroid;
        int mass;
        int values[SENSOR_COUNT];
    };

    Sensor(Settings * settings, byte s0, byte s1, byte s2, byte z);

    Sensor::Output read();
};

#endif // ifndef SENSOR_H
