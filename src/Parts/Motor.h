#ifndef MOTOR_H
#define MOTOR_H

#include <Arduino.h>

class Motor {
public:
    struct Output {
        bool forward;
        int speed;
    };

    virtual Motor::Output power(int pwm) = 0;

protected:
    byte fwd, pwm;

    Motor(byte fwd, byte pwm);
    Motor::Output normalize(int pwm);
};

class RGBMotor : public Motor {
private:
    bool anode;

public:
    RGBMotor(byte fwd, byte bwd, bool commonAnode = true);

    Motor::Output power(int pwm);
};

class DCMotor : public Motor {
public:
    DCMotor(byte phase, byte pwm);

    Motor::Output power(int pwm);
};

#endif // ifndef MOTOR_H
