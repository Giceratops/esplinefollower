#ifndef ENGINE_H
#define ENGINE_H

#include "Arduino.h"

#include "parts/Motor.h"
#include "util/Settings.h"

class Engine {
public:
    struct Output {
        int left, right;
        byte pf;
    };

    Engine(Settings * settings, Motor * left, Motor * right);

    Engine::Output power(int linePos);
    Engine::Output power(int left, int right);
    Engine::Output off();
private:
    Settings * settings;
    Motor * left, * right;
};

#endif // ifndef ENGINE_H
