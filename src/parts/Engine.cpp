    #include "Engine.h"

Engine::Engine(Settings * settings, Motor * left, Motor * right) {
    this->settings = settings;
    this->left = left;
    this->right = right;
}

Engine::Output Engine::power(int left, int right) {
    left = constrain(left, -1000, 1000);
    right = constrain(right, -1000, 1000);

    byte pf = this->settings->powerFactor();
    if (pf != 100) {
        left = (left * pf) / 100;
        right = (right * pf) / 100;
    }


    if (settings->reverse()) {
        this->left->power(left);
        this->right->power(right);
    } else {
        this->left->power(-right);
        this->right->power(-left);
    }

    return { left, right, pf };
} // Engine::power

Engine::Output Engine::power(int linePos) {
    int in = constrain(linePos, 0, 1000);
    int l, r;

    int off = settings->smoothOffset();
    int min = settings->smoothMin();

    if (in <= 500 - off) {
        l = 1000; // TODO map?
        r = map(in, 0, 500 - off, -1000, min);
    } else if (in <= 500) {
        l = 1000;
        r = map(in, 500 - off, 500, min, 1000);
    } else if (in <= 500 + off) {
        l = map(in, 500, 500 + off, 1000, min);
        r = 1000;
    } else {
        l = map(in, 500 + off, 1000, min, -1000);
        r = 1000; // TODO map?
    }

    return this->power(l, r);
} // Engine::power

Engine::Output Engine::off() {
    return this->power(0, 0);
}
