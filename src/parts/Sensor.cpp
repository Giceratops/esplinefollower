#include "Sensor.h"

Sensor::Sensor(Settings * settings, byte s0, byte s1, byte s2, byte z) {
    this->settings = settings;
    this->pins[0] = s0;
    this->pins[1] = s1;
    this->pins[2] = s2;
    this->analog = z;

    for (byte b = 0; b < 3; b++) {
        pinMode(this->pins[b], OUTPUT);
    }
};

Sensor::Output Sensor::read() {
    Sensor::Output output = {};

    int weighted = 0;
    int mass = 0;

    delay(3);
    for (byte i = 0; i < SENSOR_COUNT; i++) {
        byte sensor = SENSOR_COUNT - 1 - i;
        if (settings->reverse()) {
            sensor = i;
        }

        for (byte j = 0; j < 3; j++) {
            digitalWrite(this->pins[j], bitRead(sensor, j) == 1 ? HIGH : LOW);
        }

        int v = analogRead(this->analog);
        output.values[i] = v;

        v = _max(0, v - this->settings->calibration()[i]);
        mass += v;
        weighted += v * (i + 1);
    }
    delay(2);

    if (mass == 0) {
        output.centroid = 500;
    } else {
        output.centroid = ((weighted * 100 / mass) / (SENSOR_COUNT + 1)) * 10; // TODO * 1000 overflow?
    }
    output.mass = mass;

    return output;
};
