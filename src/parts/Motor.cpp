#include "Motor.h"

Motor::Motor(byte fwd, byte pwm) {
    this->fwd = fwd;
    this->pwm = pwm;

    pinMode(this->fwd, OUTPUT);
    pinMode(this->pwm, OUTPUT);
};

Motor::Output Motor::normalize(int pwm) {
    return {
               pwm > 0,
               map(_min(abs(pwm), 1000), 0, 1000, 0, 1023)
    };
};

RGBMotor::RGBMotor(byte fwd, byte bwd, bool commonAnode)  : Motor(fwd, bwd) {
    this->anode = commonAnode;
};

Motor::Output RGBMotor::power(int pwm) {
    Motor::Output output = this->normalize(pwm);

    if (this->anode) {
        output.speed = 1023 - output.speed;
    }

    analogWrite(this->fwd, output.forward ? output.speed : 1023);
    analogWrite(this->pwm, output.forward ? 1023 : output.speed);
    return output;
};

DCMotor::DCMotor(byte phase, byte pwm) : Motor(phase, pwm) {
};

Motor::Output DCMotor::power(int pwm) {
    Motor::Output output = this->normalize(pwm);

    digitalWrite(this->fwd, output.forward ? LOW : HIGH);
    analogWrite(this->pwm,  output.speed);

    return output;
};
