#ifndef PID_H
#define PID_H

#include "Arduino.h"

#include "util/Settings.h"

class PID {
public:
    struct Output {
        int in;
        int out;
    };

    PID(Settings * settings);

    PID::Output compute(int x);

private:
    Settings * settings;

    int errorPrior;
    double integral;

    unsigned long lastCompute;
    int lastComputed;

    bool canCompute(unsigned long delta);
    int compute(unsigned long delta, int in);
};

#endif // ifndef PID_H
