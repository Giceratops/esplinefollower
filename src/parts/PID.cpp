#include "PID.h"

PID::PID(Settings * settings) {
    this->settings = settings;
}

PID::Output PID::compute(int in) {
    int out;

    if (this->settings->pid()) {
        unsigned long now = millis();
        unsigned long delta = now - this->lastCompute;

        if (this->canCompute(delta)) {
            out = this->compute(delta, in);
            this->lastCompute = now;
        } else {
            out = this->lastComputed;
        }
    } else {
        out = in;
    }

    this->lastComputed = out;

    return { in, out };
} // PID::compute

bool PID::canCompute(unsigned long delta) {
    // sample time 4 ms.
    return delta >= 4;
}

int PID::compute(unsigned long delta, int in) {
    if (delta > 100) {
        this->errorPrior = 0;
        this->integral = 0;

        return in;
    } else {
        double dt = delta / 1000.0;

        int error = in - 500;
        double derivative = (error - this->errorPrior) / dt;

        this->integral += (error * dt);
        this->errorPrior = error;

        double pid = 0;
        pid += settings->kp() * error;
        pid += settings->ki() * this->integral;
        pid += settings->kd() * derivative;

        return constrain(in + pid, 0, 1000);
    }
} // PID::comcanCompute
