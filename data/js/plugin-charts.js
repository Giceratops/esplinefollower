(function ($, ESP, Chart) {

    `use strict`;

    Chart.defaults.global.tooltips.enabled = false;

    function createChart(selector, text) {
        let $chartArea = $(`
            <div class="chart mt-2 mb-2 websocket-open">

                <h6 class="card-subtitle text-muted">
                    Chart
                </h6>

                <p class="card-text">${text}</p>
                <canvas></canvas>
            </div>
        `);
        $(selector).find(`.card-body`).append($chartArea);
        return $chartArea;
    }

    let $sensorChart = createChart(`#card-sensor`, `Shows the relative darkness level of each sensor in %.`);
    let $pidChart = createChart(`#card-pid`, `Shows the relative position of the line (in), and where the PID steers to (out). With 0 being the center of the Line Follower.`);
    let $engineChart = createChart(`#card-engine`, `Shows the relative output of each motor in %.`);

    let chartSensor = new Chart($sensorChart.find(`canvas`), {
        type: `line`,
        data: {
            labels: [1,2,3,4,5,6,7],
            datasets: [{
                    label: `Darkness (%)`,
                    backgroundColor: `grey`,
                    borderColor: `black`,
					data:[0,0,15,80,15,0,0]
                }]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            min: 0,
                            max: 100
                        }
                    }]
            }
        }
    });

    let chartPID = new Chart($pidChart.find(`canvas`), {
        type: `line`,
        data: {
            datasets: [{
                    label: "In",
                    fill: false,
                    backgroundColor: `blue`
                }, {
                    label: "Out",
                    fill: false,
                    backgroundColor: `lightblue`
                }]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            min: -100,
                            max: 100
                        }
                    }],
                xAxes: [{
                        display: false
                    }]
            }
        }
    });

    let chartEngine = new Chart($engineChart.find(`canvas`), {
        type: `horizontalBar`,
        data: {
            datasets: [{
                    label: `Right motor (%)`,
                    backgroundColor: `green`,
                    borderColor: `black`
                }, {
                    label: `Left motor (%)`,
                    backgroundColor: `red`,
                    borderColor: `black`
                }]
        },
        options: {
            scales: {
                xAxes: [{
                        ticks: {
                            min: -100,
                            max: 100
                        }
                    }]
            }
        }
    });

    let calibration;

    ESP.addCallback({
        onReceive: function (json) {

            if (json.events && json.events.cal) {
                calibration = json.events.cal;
            }

            switch (json.mask) {
                case ESP.mask.sensor:
                    if (json.data) {
                        chartSensor.data.labels = Array(json.data.length).fill(0).map((o, i) => `${i + 1}`);
                        chartSensor.data.datasets[0].data = json.data.map((obj, i) =>
                            ((obj - calibration[i]) / (ESP.sensor.black - calibration[i])) * 100
                        );
                        chartSensor.update(0);
                    }
                    break;
                case ESP.mask.pid:
                    if (json.in !== undefined && json.out !== undefined) {
                        let data = [json.in, json.out];
                        for (let i = 0; i < data.length; i++) {
                            let set = chartPID.data.datasets[i].data || [];
                            if (set.length > 50) {
                                set.shift();
                            }
                            set.push((data[i] - 500) * 100 / 500);
                        }

                        let labels = chartPID.data.labels || [];
                        if (labels.length > 50) {
                            labels.push(labels.shift() + 50);
                        } else {
                            labels.push(labels.length);
                        }

                        chartPID.update(0);
                    }
                    break;
                case ESP.mask.engine:
                    if (json.left !== undefined && json.right !== undefined) {
                        let data = [json.left, json.right];
                        for (let i = 0; i < data.length; i++) {
                            chartEngine.data.datasets[i].data = [Math.map(
                                        data[i],
                                        ESP.motor.left.min,
                                        ESP.motor.left.max,
                                        chartEngine.options.scales.xAxes[0].ticks.min,
                                        chartEngine.options.scales.xAxes[0].ticks.max
                                        )];
                        }
                        chartEngine.update(0);
                    }
                    break;
            }
        }
    });

    //ESP.plugins.add(new ESP.Plugin(`Charts`, `<h4 class="card-title"><span class="icon icono-areaChart close"></span></h4>
    //        <h6 class="card-subtitle mb-2 text-muted">Charts</h6>` + ESP.log.info.createAlert(`Charts at least <strong>info</strong> level logging to work.`)));

})(window.$, window.ESP, window.Chart);
