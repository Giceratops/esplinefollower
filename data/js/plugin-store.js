(function(ESP, $) {

    $container = $(`<div>
        <h4 class="card-title"><span class="icon icono-dropper close"></span></h4> <br/>
        <h6 class="card-subtitle mb-2 text-muted">Preset loader <small class="form-text text-warning">Experimental</small></h6> 
    </div>`);
    
    $msg = $(`<div class="mb-2"></div>`);
    $table = $(`
        <div style="overflow: scroll">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col">File name</th>
                        <th scope="col">PF</th>
                        <th scope="col">Kp</th>
                        <th scope="col">Ki</th>
                        <th scope="col">Kd</th>
                        <th scope="col">Speed (m/s)</th>
                        <th scope="col">Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td><button class="btn btn-success btn- p-0"><span class="icono-plus"></span></button></td>
                        <td><input name="fileName" class="form-control form-control-sm" type="text" maxlength="15" /></td>
                        <td data-for="pf"></td>
                        <td data-for="kp"></td>
                        <td data-for="ki"></td>
                        <td data-for="kd"></td>
                        <td><input name="speed" class="form-control form-control-sm speed" type="number"/></td>
                        <td><input name="desc" class="form-control form-control-sm" type="text" /></td>
                    </tr>
                </tbody>
        </table>
    </div>
    `);

    $table.on(`click`, `tbody tr:first td button`, (e) => {

        $tr = $table.find(`tbody tr:first`);
        $fileName = $tr.find(`[name="fileName"]`);
        $speed = $tr.find(`[name="speed"]`);
        $desc = $tr.find(`[name="desc"]`);

        let fileName = $fileName.val() || new Date().toDateTimeString();
        let speed = $speed.val() || 0;
        let desc = encodeURI($desc.val() || `/`);

        ESP.sendMessage(`STR save /str/${fileName}.json ${speed} ${desc}`);
        addRow({
            fileName: `/str/${fileName}.json`,
            pf: $tr.find(`[data-for="pf"]`).text(),
            kp: $tr.find(`[data-for="kp"]`).text(),
            ki: $tr.find(`[data-for="ki"]`).text(),
            kd: $tr.find(`[data-for="kd"]`).text(),
            speed: speed,
            desc: desc
        });
    });

    function addRow(data) {
        $table.find(`tbody`)
            .append($(`
                <tr>
                    <td><button class="btn btn-primary btn-sm p-0" data-send="STR load ${data.fileName}"><span class="icono-check"></span></button></td>
                    <td><button class="btn btn-danger btn-sm p-0" data-send="STR rm ${data.fileName}"><span class="icono-cross"></span></button></td>
                    <td scope="row">${data.fileName.substring(5, data.fileName.length - 5)}</th>
                    <td>${data.pf}</td>
                    <td>${data.kp}</td>
                    <td>${data.ki}</td>
                    <td>${data.kd}</td>
                    <td>${data.speed}</td>
                    <td>${decodeURI(data.desc)}</td>
                </tr>
            `));
    }

    $container.append($msg);
    $container.append($table);
    $container.append($(`<button class="btn btn-primary float-right" data-send="STR">Reload</button>`));

    ESP.settings.add(new ESP.Plugin(`Storage`, $container));

    ESP.addCallback({
        onOpen: () => ESP.sendMessage(`STR`),
        onReceive : (json) => {

            if (json.str && json.str.data) {
                let $cur = $table.find(`tbody tr`).first();
                $table.find(`tbody`).html($cur);
                for (let d of json.str.data) {
                    addRow(d);
                }
            }

            if (json.str && json.str.msg) {
                if (json.str.msgType !== undefined) {
                    $msg.html(ESP.logByLevel(json.str.msgType).createAlert(json.str.msg));
                } else {
                    $msg.html(json.str.msg);
                }
            }


            if (json.str && json.str.rm) {
                $(document).find(`table [data-send="STR rm ${json.str.rm}"]`).closest(`tr`).remove();
            }
        }
    });
})(window.ESP, window.$);
