/* global VirtualJoystick, window.ESP, $ */

(function (ESP, $) {

    $(function () {
        let RADIUS = 100;
        let interval;

        let joystick = new VirtualJoystick({
            container: document.getElementById(`joystick`),
            mouseSupport: true,
            limitStickTravel: true,
            stickRadius: RADIUS,
            strokeStyle: 'slategray'
        });

        ESP.addCallback({
            onReceive: (json) => {
                if (json.events && json.events.mod !== undefined) {
                    clearInterval(interval);
                    if (json.events.mod === 2 && json.con === ESP.id()) {
                        interval = setInterval(() => {
                            let x = Math.map(-joystick.deltaX(), -RADIUS, RADIUS, -700, 700);
                            let y = Math.map(-joystick.deltaY(), -RADIUS, RADIUS, -1000, 1000);

                            ESP.sendMessage("MAN " + (y - x) + " " + (y + x));
                        }, 100);
                    }
                }
            },
            onClose: () => clearInterval(interval)
        });
    });

    let $container = $(`
            <h4 class="card-title"><span class="icon icono-mouse close"></span><span class="icon icono-keyboard close"></span></h4>
    
            <h6 class="card-subtitle mb-2 text-muted">Keyboard</h6>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Start/stop key</label>
                <div class="col-sm-3">
                    <input name="start" data-record="true" type="number" class="form-control form-control-sm" min="0" step="1" value="92">
                </div>
            </div>
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" checked>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Enable arrowkeys</span>
                    <small class="form-text text-warning ml-3">Experimental</small>
                </label>
            </div>
            
    
            <h6 class="card-subtitle mb-2 text-muted">Virtual Joystick</h6>
            <p class="card-text">
                Put the Line Follower in <kbd data-send="MOD 2">MOD 2</kbd> and use the virtual joystick in the area below to control the cart.
            </p>

            <div style="width: 100%; height: 300px; position:relative;">
                <div id="joystick" class="alert alert-secondary" style="width: calc(100% - 30px); height:100%; position:absolute; left:0;">
                </div>
                <div style="width: 300px; position:absolute; right: -140px; top: 140px;">
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control form-control-sm" type="range" value="0" min="0" max="100" data-for="pf" style="transform: rotate(270deg);"/>
                        </div>
                    </div>
                </div>
            </div>
    `);

    let $useArrows = $container.find(`input[type="checkbox"]`);
    let $startStop = $container.find(`input[type="number"]`);

    $container.find(`input[data-record="true"]`).on(`keypress`, e => e.target.value = e.which);

    ESP.plugins.add(new ESP.Plugin(`Web controller`, $container));

    $(window).keypress((e) => {
        switch (e.which) {
            case parseInt($startStop.val()):
                ESP.sendMessage(`MOD ${ESP.mod() === 0 ? 1 : 0}`);
                e.preventDefault();
                e.stopPropagation();
                break;
        }
    });

    $(window).keydown((e) => {

        if (!$useArrows.is(`:checked`)) {
            return;
        }

        switch (e.which) {
            case 37: // left
                if (ESP.mod() !== 3) {
                    ESP.sendMessage(`MOD 3`);
                }
                ESP.sendMessage(`MAN 0 1000`);
                e.preventDefault();
                e.stopPropagation();
                break;
            case 38: // up
                if (ESP.mod() !== 3) {
                    ESP.sendMessage(`MOD 3`);
                }
                ESP.sendMessage(`MAN 1000 1000`);
                e.preventDefault();
                e.stopPropagation();
                break;
            case 39: // right
                if (ESP.mod() !== 3) {
                    ESP.sendMessage(`MOD 3`);
                }
                ESP.sendMessage(`MAN 1000 0`);
                e.preventDefault();
                e.stopPropagation();
                break;
            case 40: // down
                if (ESP.mod() !== 3) {
                    ESP.sendMessage(`MOD 3`);
                }
                ESP.sendMessage(`MAN -1000 -1000`);
                e.preventDefault();
                e.stopPropagation();
                break;
        }
    });

    $(window).keyup((e) => {

        if (!$useArrows.is(`:checked`)) {
            return;
        }

        switch (e.which) {
            case 37: // left
            case 38: // up
            case 39: // right
            case 40: // down
                ESP.sendMessage(`MOD 0`);
                e.preventDefault();
                e.stopPropagation();
                break;
        }
    });


})(window.ESP, window.$);
