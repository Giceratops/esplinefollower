(function (ESP, $) {

    'use strict';

    var $plugins = $(`
        <div class="card mt-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="nav flex-column nav-pills"role="tablist" aria-orientation="vertical">
                            <div class="esp-settings"><h4 class="card-title">Settings</h4></div>
                            <div class="esp-plugins mt-3"><h4 class="card-title">Plugins</h4></div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <div class="tab-content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `);

    var $nav = $plugins.find(`.nav-pills`);
    var $content = $plugins.find(`.tab-content`);

    ESP.Plugin = function (nav, content) {

        if (typeof content === `string`) {
            content = $(`<div>${content}</div>`);
        }

        let $_nav = $(`<a class="nav-link" data-toggle="tab" href="#" role="tab">${nav}</a>`);
        let $_content = $(`<div class="tab-pane" role="tabpanel"></div>`).append(content);

        $_nav.on(`click`, function (e) {
            e.preventDefault();

            $content.find(`.tab-pane`).removeClass(`active`);
            $_content.tab('show');
        });

        this.$nav = $_nav;
        this.$content = $_content;
    };

    ESP.plugins = {};
    ESP.plugins.add = function (plugin) {
        $nav.append(plugin.$nav);
        $content.append(plugin.$content);
    };
    ESP.settings = {};
    ESP.settings.add = function (plugin) {
        $nav.find(`.esp-plugins`).before(plugin.$nav);
        $content.append(plugin.$content);
    };


    $(`#page`).append($plugins);

    // Open first tab when page is loaded.
    $(function () {
        $nav.find(`[data-toggle="tab"]:first`).click();
    });
})(window.ESP, window.$);
