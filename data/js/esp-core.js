window.ESP = {};

(function (ESP, JSON) {

    `use strict`;

    let _id = -1;
    let _mod = 0;
    let _connection = false;
    let _callbacks = {
        onConnect: [],
        onOpen: [],
        onClose: [],
        onError: [],
        onSend: [],
        onReceive: []
    };

    let callback = function (callbacks) {
        return function (arg) {
            callbacks.filter(callback => typeof callback === `function`)
                    .forEach(callback => callback(arg));
        };
    };

    ESP.id = () => _id;
    ESP.mod = () => _mod;
    ESP.isConnected = () => _connection && _connection.readyState === _connection.OPEN;

    ESP.connect = function () {
        this.disconnect();

        callback(_callbacks.onConnect)();

        _connection = new WebSocket(`ws://${document.location.host}/ws`, [`arduino`]);

        _connection.onopen = callback(_callbacks.onOpen);
        _connection.onclose = function () {
            _id = -1;

            callback(_callbacks.onClose)(arguments);
        };
        _connection.onerror = callback(_callbacks.onError);
        _connection.onmessage = (event) => {
            try {
                let json = JSON.parse(event.data);
                
                if (json.events !== undefined
                        && json.events.mod !== undefined) {
                    _mod = json.events.mod;
                }

                callback(_callbacks.onReceive)(json);

                if (json.handshake) {
                    _id = json.con;
                }
            } catch (e) {
                ESP.terminals.browser.log(ESP.log.error, `Error pasrsing JSON: ${event.data}`);
                console.log("Error pasrsing JSON", e, event.data);
            }
        };
    };

    ESP.mockMessage = function (message) {
        _connection.onmessage({data: message});
    };

    ESP.disconnect = function () {
        if (this.isConnected()) {
            _connection.close();
        }
    };

    ESP.addCallback = function (callbacks) {
        for (let callback in _callbacks) {
            if (callbacks[callback]) {
                let tmp = callbacks[callback];
                if (tmp instanceof Array) {
                    _callbacks[callback].push(...tmp);
                } else {
                    _callbacks[callback].push(tmp);
                }
            }
        }
    };

    ESP.sendMessage = function(msg) {
        let open = this.isConnected();
        if (open) {
            _connection.send(msg);
        }
        callback(_callbacks.onSend)({
            open: open,
            data: msg
        });
        return open;
    };

    window.addEventListener(`load`, () => ESP.connect());

})(window.ESP, window.JSON);
