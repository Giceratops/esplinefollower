(function ($, ESP) {

    `use strict`;

    let WAIT_TIME = 100;

    $(document).on(`click`, `.btn-connect`, function (e) {
        ESP.connect();
    });
    
    let dataChanges = {};
    $(document).on(`input change`, `[data-for]`, function (e) {
        let $this = $(this);
        let val;
        if ($this.is(`[type="checkbox"]`)) {
            val = this.checked ? 1 : 0;
        } else {
            val = $this.val();
        }

        let now = Date.now();
        let type = $this.data(`for`);
        let data = dataChanges[type] || {time: 0, value: null};
        $(`[data-for="${type}"]`).toArray()
                .filter(e => !$this.is(e))
                .forEach(e => {
                    e.value = val;
                    e.innerHTML = val;
                });
        if (data.value !== val && (e.type === `change` || now > data.time + WAIT_TIME)) {
            dataChanges[type] = {time: now, value: val};
            ESP.sendMessage(`SET ${type} ${val}`);
        }
    });

    $(document).on(`click`, `[data-send]`, function (e) {
        let $this = $(this);
        if ($this.is(`.btn-start`)) {
            $this.text(`${$this.text().trim()}ing`);
            $this.attr(`disabled`, `disabled`);
        }

        ESP.sendMessage($this.data('send'));
    });

    $(document).on('change', '[data-log-flag]', function (e) {
        var $this = $(this);

        var tmp = logLevel;
        var pos = Math.log2(parseInt($this.data('log-flag'))) + 1;
        tmp = parseInt(tmp / Math.pow(10, pos)) * Math.pow(10, pos) + tmp % Math.pow(10, pos - 1);
        tmp += Math.pow(10, pos - 1) * this.selectedIndex;

        if (tmp !== logLevel) {
            logLevel = tmp;
            ESP.sendMessage('SET log ' + tmp);
        }
    });

    let logLevel = 0;
    ESP.addCallback({
        onConnect: function () {
            $(`html`).attr(`data-websocket`, `opening`);
        },
        onOpen: function () {
            $(`html`).attr(`data-websocket`, `open`);
        },
        onClose: function () {
            $(`html`).attr(`data-websocket`, `closed`);
        },
        onReceive: function (json) {

            if (json.events) {
                $.each(json.events, (type, value) => {
                    if (type === `mod`) {
                        let $btn = $(`.btn-start`).removeAttr(`disabled`);
                        let classStart = `btn-success`;
                        let classStop = `btn-danger`;
                        if (value === 0) {
                            $btn.text(`Start`)
                                    .data(`send`, `MOD 1`)
                                    .removeClass(classStop)
                                    .addClass(classStart);
                        } else {
                            $btn.text(`Stop`)
                                    .data(`send`, `MOD 0`)
                                    .removeClass(classStart)
                                    .addClass(classStop);
                        }

                    } else if (type === 'log') {
                        logLevel = value;
                        $.each($('[data-log-flag]'), function (i, obj) {
                            var pos = Math.log2(parseInt($(obj).data('log-flag'))) + 1;
                            obj.selectedIndex = (logLevel / Math.pow(10, pos - 1)) % 10;
                        });
                    } else {

                        let $elems = $(`[data-for="${type}"]`);
                        if (ESP.id() !== json.con || !$elems.toArray().find(e => $(e).is(`:focus`))) {
                            $.each($elems, (i, el) => {
                                let $elem = $(el);
                                if ($elem.is(`input`)) {
                                    if ($elem.is(`[type="checkbox"]`)) {
                                        if (value) {
                                            $elem.attr(`checked`, `checked`);
                                        } else {
                                            $elem.removeAttr(`checked`);
                                        }
                                    } else {
                                        $elem.val(value);
                                    }
                                } else {
                                    $elem.text(value);
                                }
                            });
                        }
                    }
                });
            }
        }
    });
})(window.$, window.ESP);
