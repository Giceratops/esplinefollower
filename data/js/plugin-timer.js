(function (ESP, $) {

    `use strict`;

    let last = new Date();
    let detections = 0;
    let laps = 0;

    ESP.addCallback({onReceive: (json) => {
            if (json.mask === ESP.mask.sensor) {
                let now = new Date();
                if (now.getTime() > last.getTime() + 1000) {
                    if (json.massMax > $crossThreshold.val()) {
                        detections++;

                        if (detections % $crossroads.val() === 0) {
                            let time = now - last;
                            let speed = ($trackLength.val() / time).toFixed(3);
                            
                            $tableBody.prepend($(`
                                        <tr>
                                            <th>${++laps}</th>
                                            <td>${(time / 1000).toFixed(3)}</td>
                                            <td>${speed}</td>
                                        </tr>`
                                    ));

                            $.each($(`.speed`), (i, el) => {
                                let $el = $(el);
                                if ($el.is(`input`)) {
                                    $el.val(speed);
                                } else {
                                    $el.text(speed);
                                }
                            });
                            last = now;
                            detections = 0;
                        }
                    }
                }
            }
        }});

    let $container = $(`
        <div>
            <h4 class="card-title"><span class="icon icono-reset close"></span></h4>
            <h6 class="card-subtitle mb-2 text-muted">Track timer</h6>
    
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Track length (mm)</label>
                <div class="col-sm-3">
                    <input name="track" type="number" class="form-control form-control-sm" min="0" step="1" value="4144">
                </div>
                <label class="col-sm-3 col-form-label">Cross count</label>
                <div class="col-sm-3">
                    <input name="count" type="number" class="form-control form-control-sm" min="0" step="1" value="2">
                </div>
                <label class="col-sm-3 col-form-label">Crossroad threshold</label>
                <div class="col-sm-3">
                    <input name="threshold" type="number" class="form-control form-control-sm" min="0" step="1" value="3500">
                </div>
            </div>
    
            <div style="max-height: 250px; overflow-y: scroll;">
                <table class="table table-sm table-striped">
                    <thead>
                        <tr>    
                            <th scope="col">Lap</th>
                            <th scope="col">Time (s)</th>
                            <th scope="col">Speed (m/s)</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    `);

    let $crossroads = $container.find(`input[name="count"]`);
    let $trackLength = $container.find(`input[name="track"]`);
    let $crossThreshold = $container.find(`input[name="threshold"]`);
    let $tableBody = $container.find(`table tbody`);

    ESP.plugins.add(new ESP.Plugin(`Timer`, $container));

})(window.ESP, window.$);
