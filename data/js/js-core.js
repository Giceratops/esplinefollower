(function(){

    function zeroPad(number, length) {
        number = '' + number;

        if (number.length < length) {
            return zeroPad('0' + number, length);
        } else {
            return number;
        }
    }

    Date.prototype.toLogString = function () {
        return zeroPad(this.getHours(), 2) + ':'
                + zeroPad(this.getMinutes(), 2) + ':'
                + zeroPad(this.getSeconds(), 2) + '.'
                + zeroPad(this.getMilliseconds(), 3);
    };

    Date.prototype.toDateTimeString = function() {
        return zeroPad(this.getDate(), 2)
                + zeroPad(this.getMonth(), 2) + `-`
                + zeroPad(this.getHours(), 2)
                + zeroPad(this.getMinutes(), 2)
                + zeroPad(this.getSeconds(), 2);
    }

    Math.map = function (x, inMin, inMax, outMin, outMax) {
        return parseInt((x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin);
    };
})()
