(function (ESP) {

    ESP.sensor = {white: 0, black: 1000};

    ESP.pid = {min: 0, max: 1000};

    ESP.motor = {
        left: {min: -1000, max: 1000},
        right: {min: -1000, max: 1000}
    };

    ESP.LogLevel = function (name, style) {
        this.name = name;
        this.style = style;

        this.createAlert = function (html) {
            return '<div class="alert alert-' + this.style + ' p-0">' +
                    '<span class="badge badge-' + this.style + ' float-right">' + this.name + '</span>' +
                    '<span class="ml-2">' + html + '</span>' +
                    '</div>';
        };
    };

    ESP.log = {
        off: new ESP.LogLevel(`Off`, ` hidden`),
        error: new ESP.LogLevel(`Error`, `danger`),
        warning: new ESP.LogLevel(`Warning`, `warning`),
        info: new ESP.LogLevel(`Info`, `info`),
        debug: new ESP.LogLevel(`Debug`, `dark`),
        fine: new ESP.LogLevel(`Verbose`, `secondary`)
    };

    ESP.logByLevel = function (level) {
        return ESP.log[Object.keys(ESP.log)[level]];
    };

    ESP.mask = {
        event: 1 << 0,
        main: 1 << 1,
        sensor: 1 << 2,
        pid: 1 << 3,
        engine: 1 << 4
    };

    ESP.maskByLevel = function (level) {
        let entry = Object.entries(ESP.mask)
                .find(e => e[1] === level);

        return entry ? entry[0] : undefined;
    };


})(window.ESP);
