(function (ESP, $) {

    function quadToNumber(ip) {
        if (!(ip instanceof  Array)) {
            ip = ip.split(`.`);
        }
        return ip.reduce((prev, elem, index) => prev |= elem << (8 * index), 0);
    }

    function numberToQuad(ip) {
        let res = "";
        for (let i = 3; i >= 0; i--) {
            let j = ip >> (8 * i);
            ip -= j << (8 * i);
            res = "." + j + res;
        }

        return res.substring(1);
    }

    `use strict`;

    // --- ESP -------------------------------------------------------------- //

    let formESP = $(`
        <div>
            <h4 class="card-title"><span class="icon icono-tiles close"></span></h4>
            <h6 class="card-subtitle mb-2 text-muted">Hardware</h6>
            <div class="form-group">
                <div class="input-group">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-for="pullup">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Pull-up buttons</span> 
                    </label>
                    <small class="form-text text-warning">Requires a restart</small>
                </div>

                <div class="input-group">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-for="reverse">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Reverse forward</span>
                    </label>
                </div>
            </div>

            <h6 class="card-subtitle mb-2 mt-2 text-muted">Software</h6>
            <div class="form-group">
                <div class="input-group">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-for="usepid">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Use PID</span>
                    </label>
                </div>
            </div>
    
            <h6 class="card-subtitle mb-2 mt-2 text-muted">Driving style</h6>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Forward gutter</label>
                <div class="col-sm-3">
                    <input data-for="smoothoffset" type="number" class="form-control form-control-sm" min="0" max="1000" step="1">
                </div>
                <label class="col-sm-3 col-form-label">Minimal forward speed</label>
                <div class="col-sm-3">
                    <input data-for="smoothmin" type="number" class="form-control form-control-sm" min="-1000" max="1000" step="1">
                </div>
            </div>
    
            <h6 class="card-subtitle mb-2 mt-2 text-muted">Autostop</h6>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Low threshold</label>
                <div class="col-sm-3">
                    <input data-for="lowthreshold" type="number" class="form-control form-control-sm" min="0" max="1000" step="1">
                </div>
                <label class="col-sm-3 col-form-label">Low detections</label>
                <div class="col-sm-3">
                    <input data-for="lowdetections" type="number" class="form-control form-control-sm" min="0" step="1">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">High threshold</label>
                <div class="col-sm-3">
                    <input data-for="highthreshold" type="number" class="form-control form-control-sm" min="0" max="1000" step="1">
                </div>
                <label class="col-sm-3 col-form-label">High detections</label>
                <div class="col-sm-3">
                    <input data-for="highdetections" type="number" class="form-control form-control-sm" min="0" step="1">
                </div>
            </div>

            <h6 class="card-subtitle mb-2 mt-2 text-muted">Logging</h6>
            <div class="form-group">
            </div>
        </div>`);

    Object.keys(ESP.mask).forEach(e => {
        let $option = $(`
            <div class="row">
                <div class="col-3">
                                <span class="text-capitalize">${e}</span>
                        </div>
                <div class="input-group col-9 mb-1">
                    <select class="form-control form-control-sm custom-select" data-log-flag="${ESP.mask[e]}"></select>
                </div>
            </div>
        `);
        Object.values(ESP.log).forEach(e => {
            $option.find(`.form-control`).append($(`<option>${e.name}</option>`));
        });
        formESP.find(`.form-group`).last().append($option);
    });

    ESP.settings.add(new ESP.Plugin(`ESP`, formESP));

    ESP.addCallback({
        onReceive: function (json) {
            if (json.events && json.events.usepid !== undefined) {
                let elems = $(`[data-for="kp"], [data-for="ki"], [data-for="kd"]`);
                if (json.events.usepid) {
                    elems.removeAttr(`disabled`);
                } else {
                    elems.attr(`disabled`, `disabled`);
                }
            }
        }
    });

    // --- Network ---------------------------------------------------------- //

    let $formNetwork = $(`
        <form novalidate>
            <h4 class="card-title"><span class="icon icono-rss close"></span></h4> <br/>
            <h6 class="card-subtitle mb-2 text-muted">Acces point configuration</h6>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Host name</label>
                <div class="col-sm-10">
                    <input name="hostName" required type="text" class="form-control form-control-sm" placeholder="esp-async" pattern="[^' ']+">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">SSID</label>
                <div class="col-sm-10">
                    <input name="ssid" required type="text" class="form-control form-control-sm" placeholder="ESP-LF" pattern="[^' ']+">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    <input name="pass" type="password" class="form-control form-control-sm" placeholder="" pattern=".{0}|.{8,}|[^' ']+">
                    <small class="form-text text-muted">Empty or 8 or more characters</small>
                </div>
                
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Gateway</label>
                <div class="col-sm-10">
                    <input name="gateway" required type="text" class="form-control form-control-sm" placeholder="192.168.1.1" pattern="(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])">
                </div>
            </div>
    
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">IP</label>
                <div class="col-sm-10">
                    <input name="ip" required type="text" class="form-control form-control-sm" placeholder="192.168.1.3" pattern="(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Mask</label>
                <div class="col-sm-10">
                    <input name="mask" required type="text" class="form-control form-control-sm" placeholder="255.255.255.0" pattern="(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Channel</label>
                <div class="col-sm-10">
                    <input name="channel" required type="number" class="form-control form-control-sm" placeholder="11" min="1" max="13">
                </div>
            </div>
    
            <div class="form-group float-right">
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                <small class="form-text text-warning">Requires a restart</small>
            </div>
        </form>
    `);

    $formNetwork.on(`submit`, (e) => {
        e.preventDefault();
        e.stopPropagation();
        e.target.classList.add('was-validated');

        if (e.target.checkValidity()) {
            let data = {};
            $.each($formNetwork.find(`input`), (i, el) => {
                if ((el.value + ``).indexOf(`.`) > 0) {
                    data[el.name] = quadToNumber(el.value);
                } else {
                    data[el.name] = el.value;
                }
            });

            ESP.sendMessage(`SET ip ${JSON.stringify(data)}`);
        }
    });

    ESP.addCallback({onReceive: (json) => {
            if (json.events !== undefined && json.events.ip !== undefined) {
                $.each(json.events.ip, (i, el) => {
                    if ([`mask`, `ip`, `gateway`].indexOf(i) > -1) {
                        el = numberToQuad(el);
                    }
                    $formNetwork.find(`[name="${i}"]`).val(el);
                });
            }
        }});

    ESP.settings.add(new ESP.Plugin(`Network`, $formNetwork));

})(window.ESP, window.$);

