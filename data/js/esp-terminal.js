(function (ESP, $) {

    'use strict';

    ESP.terminals = {
        esp: new Terminal(`#terminal-esp`),
        network: new Terminal(`#terminal-network`),
        browser: new Terminal(`#terminal-browser`)
    };

    $(`.terminal-toggle`).on(`click`, function () {
        var $html = $(`html`);
        $html.attr(`data-terminal`, $html.attr(`data-terminal`) === `open` ? `closed` : `open`);
    });

    $(`.terminal .icono-trash`).on(`click`, function () {
        Object.values(ESP.terminals).forEach(e => e.clear());
    });

    $('#terminal-input').on('change', function (e) {
        var $this = $(this);
        var val = $this.val();

        ESP.sendMessage(val);

        $this.val('');
    });


    function Terminal(terminal) {

        let _$terminal = $(terminal);

        this.clear = function () {
            _$terminal.text(``);
        };

        this.log = function (level, text) {
            var row = $(`
                <div class="row log">
                    <div class="col col-sm-2 col-lg-1">
                        <span class="badge badge-${level.style}">${level.name}</span>
                    </div>

                    <div class="col-12 col-sm order-12 order-sm-2">
                        ${text}
                    </div>
                    
                    <div class="col-auto order-2 order-sm-12 text-muted ">
                        ${new Date().toLogString()}
                    </div>
                </div>
            `);

            _$terminal.prepend(row);

            var children = _$terminal.children();
            if (children.length > 100) {
                children[100].remove();
            }
        };
    }
    ;

    let LOG_RECV = new ESP.LogLevel(`Recv`, `danger`);
    let LOG_SEND = new ESP.LogLevel(`Send`, `success`);
    let LOG_EVENT = new ESP.LogLevel(`Event`, `primary`);

    ESP.addCallback({
        onConnect: function () {
            ESP.terminals.browser.log(LOG_EVENT, 'WebSocket <span class="badge badge-pill badge-warning">connecting</span>');
        },
        onOpen: function () {
            ESP.terminals.browser.log(LOG_EVENT, 'WebSocket <span class="badge badge-pill badge-success">connected</span>');
        },
        onClose: function () {
            ESP.terminals.browser.log(LOG_EVENT, 'WebSocket <span class="badge badge-pill badge-danger">disconnected</span>');
        },
        onError: function (error) {
            console.log(error);

            ESP.terminals.browser.log(ESP.log.error, 'WebSocket error (see console for more details)');
        },
        onReceive: function (json) {
            ESP.terminals.network.log(LOG_RECV, JSON.stringify(json));

            if (json.events) {
                $.each(json.events, function (type, value) {
                    ESP.terminals.esp.log(LOG_EVENT, type + ' = ' + value);
                });
            }

            if (json.msg) {
                ESP.terminals.esp.log(ESP.logByLevel(json.level), `${ESP.maskByLevel(json.mask)}: ${json.msg}`);
            }
        },
        onSend: function (e) {
            if (!e.open) {
                e.data = `<span class="badge badge-pill badge-danger">Connection closed</span> ${e.data}`;
            }

            ESP.terminals.network.log(LOG_SEND, e.data);
        }
    });

})(window.ESP, window.$);

